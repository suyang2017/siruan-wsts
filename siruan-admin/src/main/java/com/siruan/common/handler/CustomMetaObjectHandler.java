package com.siruan.common.handler;

import com.baomidou.mybatisplus.mapper.MetaObjectHandler;
import com.siruan.modules.sys.shiro.ShiroUtils;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CustomMetaObjectHandler extends MetaObjectHandler {
    //新增填充
    @Override
    public void insertFill(MetaObject metaObject) {
        System.out.printf("insertFill.................");
        Object createTime = getFieldValByName("createTime", metaObject);
        if(createTime == null){
            setFieldValByName("createTime",  new Date(), metaObject);
        }
        Object createUser = getFieldValByName("createUser", metaObject);
        if(createUser == null){
            setFieldValByName("createUser", ShiroUtils.getUserId(), metaObject);
        }
    }

    //更新填充
    @Override
    public void updateFill(MetaObject metaObject) {
        System.out.printf("updateFill.................");
        Object updateTime = getFieldValByName("updateTime", metaObject);
        if(updateTime == null){
            setFieldValByName("updateTime",  new Date(), metaObject);
        }
        Object updateUser = getFieldValByName("updateUser", metaObject);
        if(updateUser == null){
            setFieldValByName("updateUser",  ShiroUtils.getUserId(), metaObject);
        }
    }
}
