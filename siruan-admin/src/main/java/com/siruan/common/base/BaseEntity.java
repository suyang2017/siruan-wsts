package com.siruan.common.base;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldFill;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public abstract class BaseEntity<T extends Model> extends Model<T> implements Serializable {

    private static final long serialVersionUID = 1L;
    /*
     * 创建人
     */

    @TableField(value = "create_user", fill = FieldFill.INSERT)
    protected Long createUser;
    /*
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    protected Date createTime;
    /*
     * 最后修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    protected Date updateTime;
    /*
     * 最后修改人
     */
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    protected Long updateUser;
}

