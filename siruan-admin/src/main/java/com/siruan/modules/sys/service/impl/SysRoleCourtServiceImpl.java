/**
 * Copyright 2018 人人开源 http://www.siruan.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.siruan.modules.sys.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.siruan.modules.sys.dao.SysRoleCourtDao;
import com.siruan.modules.sys.entity.SysRoleCourtEntity;
import com.siruan.modules.sys.service.SysRoleCourtService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * 角色与法院对应关系
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017年6月21日 23:42:30
 */
@Service("sysRoleCourtService")
public class SysRoleCourtServiceImpl extends ServiceImpl<SysRoleCourtDao, SysRoleCourtEntity> implements SysRoleCourtService {

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveOrUpdate(Long roleId, Long courtCode) {
		//先删除角色与法院关系
		deleteBatch(new Long[]{roleId});

		// 保存角色和法院的关系
		SysRoleCourtEntity sysRoleCourtEntity = new SysRoleCourtEntity();
		sysRoleCourtEntity.setCourtCode(courtCode);
		sysRoleCourtEntity.setRoleId(roleId);
		this.insert(sysRoleCourtEntity);
	}

	@Override
	public Long queryCourtCode(Long[] roleIds) {
		return baseMapper.queryCourtCode(roleIds);
	}

	@Override
	public int deleteBatch(Long[] roleIds){
		return baseMapper.deleteBatch(roleIds);
	}
}
