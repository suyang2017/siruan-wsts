package com.siruan.modules.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.siruan.common.annotation.SysLog;
import com.siruan.common.validator.ValidatorUtils;
import com.siruan.modules.sys.entity.SysMenuEntity;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.siruan.modules.sys.entity.SysCourtEntity;
import com.siruan.modules.sys.service.SysCourtService;
import com.siruan.common.utils.PageUtils;
import com.siruan.common.utils.R;



/**
 * 法院管理
 *
 * @author sy
 * @email sy@gmail.com
 * @date 2019-01-01 14:55:24
 */
@RestController
@RequestMapping("sys/court")
public class SysCourtController {
    @Autowired
    private SysCourtService sysCourtService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:court:list")
    public R list(){
        List<SysCourtEntity> courtEntityList = sysCourtService.selectList(null);
        for(SysCourtEntity sysCourtEntity : courtEntityList){
            SysCourtEntity parentCourtEntity = sysCourtService.selectOne(new EntityWrapper<SysCourtEntity>().eq("court_code", sysCourtEntity.getSuperiorCode()));
            if(parentCourtEntity != null){
                sysCourtEntity.setSuperiorName(parentCourtEntity.getName());
            }
        }

        return R.ok().put("data", courtEntityList);
    }


    /**
     * 选择菜单(添加、修改菜单)
     */
    @RequestMapping("/select")
    @RequiresPermissions("sys:court:select")
    public R select(){
        //查询列表数据
        List<SysCourtEntity> courtList = sysCourtService.selectList(new EntityWrapper());
        return R.ok().put("data", courtList);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{courtCode}")
    @RequiresPermissions("sys:court:info")
    public R info(@PathVariable("courtCode") Long courtCode){
        SysCourtEntity sysCourt = sysCourtService.selectById(courtCode);

        return R.ok().put("data", sysCourt);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:court:save")
    public R save(@RequestBody SysCourtEntity sysCourt){
        sysCourtService.insert(sysCourt);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:court:update")
    public R update(@RequestBody SysCourtEntity sysCourt){
        ValidatorUtils.validateEntity(sysCourt);
        sysCourtService.updateAllColumnById(sysCourt);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete/{}")
    @RequiresPermissions("sys:court:delete")
    public R delete(@RequestBody Long[] courtCodes){
        sysCourtService.deleteBatchIds(Arrays.asList(courtCodes));

        return R.ok();
    }
    /**
     * 删除
     */
    @SysLog("删除菜单")
    @RequestMapping("/delete/{courtCode}")
    @RequiresPermissions("sys:court:delete")
    public R delete(@PathVariable("courtCode") long courtCode){

        //判断是否有子菜单或按钮
        List<SysCourtEntity> courtList = sysCourtService.queryListSuperiorCode(courtCode);
        if(courtList.size() > 0){
            return R.error("请先删除下级法院");
        }

        sysCourtService.delete(courtCode);

        return R.ok();
    }

}
