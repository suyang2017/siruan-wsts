package com.siruan.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.siruan.common.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

import static com.baomidou.mybatisplus.enums.IdType.INPUT;

/**
 * 法院管理
 * 
 * @author sy
 * @email sy@gmail.com
 * @date 2019-01-01 14:55:24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_court")
public class SysCourtEntity extends BaseEntity<SysCourtEntity> {
	private static final long serialVersionUID = 1L;

	/**
	 * 法院代码
	 */
	@TableId(type = INPUT)
	private Long courtCode;
	/**
	 * 上级码
	 */
	private Long superiorCode;
	/**
	 * 父菜单名称
	 */
	@TableField(exist=false)
	private String superiorName;
	/**
	 * 分级码
	 */
	private Long hierarchicalCode;
	/**
	 * 机关代字
	 */
	private String organSubstitution;
	/**
	 * 法院名称
	 */
	private String name;
	/**
	 * 法院介绍
	 */
	private String introduce;
	/**
	 * 状态（禁用、启用）
	 */
	private Integer status;

	@Override
	protected Serializable pkVal() {
		return this.courtCode;
	}
}
