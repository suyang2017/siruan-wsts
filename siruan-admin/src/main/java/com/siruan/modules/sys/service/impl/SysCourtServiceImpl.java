package com.siruan.modules.sys.service.impl;

import com.siruan.common.annotation.DataFilter;
import com.siruan.common.utils.Constant;
import com.siruan.common.utils.MapUtils;
import com.siruan.modules.sys.entity.SysCourtEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.siruan.common.utils.PageUtils;
import com.siruan.common.utils.Query;

import com.siruan.modules.sys.dao.SysCourtDao;
import com.siruan.modules.sys.entity.SysCourtEntity;
import com.siruan.modules.sys.service.SysCourtService;


@Service("sysCourtService")
public class SysCourtServiceImpl extends ServiceImpl<SysCourtDao, SysCourtEntity> implements SysCourtService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysCourtEntity> page = this.selectPage(
                new Query<SysCourtEntity>(params).getPage(),
                new EntityWrapper<SysCourtEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    @DataFilter(subCourt = true, user = false)
    public List<SysCourtEntity> queryList(Map<String, Object> params){
        List<SysCourtEntity> deptList =
                this.selectList(new EntityWrapper<SysCourtEntity>()
                        .addFilterIfNeed(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER)));

        for(SysCourtEntity sysCourtEntity : deptList){
            SysCourtEntity parentDeptEntity =  this.selectById(sysCourtEntity.getCourtCode());
            if(parentDeptEntity != null){
                sysCourtEntity.setSuperiorName(parentDeptEntity.getName());
            }
        }
        return deptList;
    }

    @Override
    public void delete(Long courtCode) {
        //删除法院
        this.deleteById(courtCode);
    }

    @Override
    public List<SysCourtEntity> queryListSuperiorCode(long courtCode) {
        return baseMapper.queryListSuperiorCode(courtCode);

    }

}
