package com.siruan.modules.sys.dao;

import com.siruan.modules.sys.entity.SysCourtEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * 法院管理
 * 
 * @author sy
 * @email sy@gmail.com
 * @date 2019-01-01 14:55:24
 */
public interface SysCourtDao extends BaseMapper<SysCourtEntity> {

    List<SysCourtEntity> queryListSuperiorCode(long courtCode);
}
