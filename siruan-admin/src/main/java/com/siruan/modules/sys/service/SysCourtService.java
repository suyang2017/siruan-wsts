package com.siruan.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.siruan.common.utils.PageUtils;
import com.siruan.modules.sys.entity.SysCourtEntity;
import com.siruan.modules.sys.entity.SysDeptEntity;

import java.util.List;
import java.util.Map;

/**
 * 法院管理
 *
 * @author sy
 * @email sy@gmail.com
 * @date 2019-01-01 14:55:24
 */
public interface SysCourtService extends IService<SysCourtEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<SysCourtEntity> queryList(Map<String, Object> map);

    /**
     * 删除
     */
    void delete(Long courtCode);

    List<SysCourtEntity> queryListSuperiorCode(long courtCode);
}

