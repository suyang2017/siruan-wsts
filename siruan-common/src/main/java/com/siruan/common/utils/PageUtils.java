/**
 * Copyright 2018 人人开源 http://www.siruan.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.siruan.common.utils;

import com.baomidou.mybatisplus.plugins.Page;

import java.io.Serializable;
import java.util.List;

/**
 * 分页工具类
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年11月4日 下午12:59:00
 */
public class PageUtils implements Serializable {
	private static final long serialVersionUID = 1L;
	//总记录数
	private int total;
	//每页记录数
	private int limit;
	//总页数
	private int totalPage;
	//当前页数
	private int page;
	//列表数据
	private List<?> list;
	
	/**
	 * 分页
	 * @param list        列表数据
	 * @param total  总记录数
	 * @param limit    每页记录数
	 * @param page    当前页数
	 */
	public PageUtils(List<?> list, int total, int limit, int page) {
		this.list = list;
		this.total = total;
		this.limit = limit;
		this.page = page;
		this.totalPage = (int)Math.ceil((double)total/limit);
	}

	/**
	 * 分页
	 */
	public PageUtils(Page<?> page) {
		this.list = page.getRecords();
		this.total = page.getTotal();
		this.limit = page.getSize();
		this.page = page.getCurrent();
		this.totalPage = page.getPages();
	}

	public int gettotal() {
		return total;
	}

	public void settotal(int total) {
		this.total = total;
	}

	public int getlimit() {
		return limit;
	}

	public void setlimit(int limit) {
		this.limit = limit;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getpage() {
		return page;
	}

	public void setpage(int page) {
		this.page = page;
	}

	public List<?> getList() {
		return list;
	}

	public void setList(List<?> list) {
		this.list = list;
	}
	
}
